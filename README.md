

## Installing OpenCV on Mac

Install cmake using home brew:

brew install cmake

Clone the git repository locally:

git clone https://github.com/opencv/opencv.git opencv

mkdir build

cd build

ccmake ../

make

If all good:

make install

Configure for Release builds

## Adding OpenCV jar to local maven repository

http://roufid.com/3-ways-to-add-local-jar-to-maven-project/

mvn install:install-file -Dfile=/usr/local/share/OpenCV/java/opencv-330.jar \
 -DgroupId=opencv \
 -DartifactId=opencv-java \
 -Dversion=3.3.0 \
 -DgeneratePom=true \
 -Dpackaging=jar
 
 mvn install:install-file -Dfile=libs/TarsosDSP-2.4.jar \
 -DgroupId=tarsos \
 -DartifactId=tarsos-dsp \
 -Dversion=0.1 \
 -DgeneratePom=true \
 -Dpackaging=jar

## Create a face collection

 aws rekognition create-collection --collection-id receptionist --profile receptionist --region us-east-1

## Drop an existing face collection

  aws rekognition delete-collection --collection-id receptionist --profile receptionist --region us-east-1

