package com.aws.sa.receptionist.config;

public class Config {
    public static boolean fullscreen = false;
    public static String AWS_KEY = "AKIAIFN653TLE2NIWCYQ";
    public static String AWS_SECRET = "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";
    public static String LEX_NAME = "Joe";
    public static String LEX_ALIAS = "GBSTJoe";
    public static String ASSISTANT_NAME = "Syndy";
    public static String ASSISTANT_SCREEN_NAME = "Syndy";
    public static String VOICE = "Amy";
    public static String LISTENING = "I'm listening...";
    public static String THINKING = "Let me see...";
    public static String PROMPT_TO_ASK = "Click microphone to ask.";
    public static String PROMPT_TO_ASK_WALKIE_TALKIE_MODE = "Press and Hold microphone to ask.";
    public static String APP_NAME = "GBST Digital Assistant";
    public static String WELCOME = "<speak>Welcome to " + APP_NAME + ".</speak>";
    public static String GREETING = "<speak>I'm " + Config.ASSISTANT_NAME + ". How can I help?</speak>";
    public static String PARDON = "<speak>I beg your pardon.</speak>";
    public static int MAX_SENTENCE_LENGTH_MILLI_SECONDS = 14500;
    public static int MAX_SILENCE_MILLI_SECONDS = 1000;
    public static double SOUND_LEVEL_SILENCE = -68.0;
    public static int HEARTBEAT_INTERVAL_MINUTES = 5;
    public static int PAUSE_BEFORE_FINISH_MILLI_SECONDS = 500;
    public static boolean WALKIE_TALKIE_MODE = true;
}
