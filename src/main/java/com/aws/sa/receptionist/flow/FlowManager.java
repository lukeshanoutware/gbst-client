package com.aws.sa.receptionist.flow;

import com.aws.sa.receptionist.context.StateBot;
import com.aws.sa.receptionist.context.UserContext;
import com.aws.sa.receptionist.listen.Joe;
import org.apache.log4j.Logger;

public class FlowManager {
    @SuppressWarnings("unused")
    private static final Logger LOGGER = Logger.getLogger(FlowManager.class);

    public static void run(UserContext context) {
        final StateBot joe = new Joe(context, null);
        joe.run();
    }
}
