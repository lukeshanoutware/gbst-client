package com.aws.sa.receptionist.util

import kotlinx.coroutines.experimental.*
import kotlin.coroutines.experimental.CoroutineContext
import kotlin.coroutines.experimental.EmptyCoroutineContext

fun cancelJob(job: Job?, cause: Throwable? = null) = job?.cancel(cause)

fun launchNewScopeUIBlock(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job = launchNewScopeBlock(true, coroutineName, completionHandler, block)

fun launchNewScopeIOBlock(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job = launchNewScopeBlock(false, coroutineName, completionHandler, block)

fun CoroutineScope.launchUIBlock(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job = launchBlock(true, coroutineName, completionHandler, block)

fun CoroutineScope.launchIOBlock(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job = launchBlock(false, coroutineName, completionHandler, block)

fun <T> CoroutineScope.asyncIOBlock(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> T): Deferred<T> {
    val dispatcher = coroutineName?.let { Dispatchers.IO + CoroutineName(coroutineName) } ?: Dispatchers.IO
    return async(dispatcher) {
        block()
    }.also { deferred ->
        completionHandler?.let {
            deferred.invokeOnCompletion(it)
        }
    }
}

suspend fun <T> CoroutineScope.asyncIOBlockAwait(coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> T): T = asyncIOBlock(coroutineName, completionHandler, block).await()

private fun launchNewScopeBlock(uiDispatcher: Boolean = true, coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job = Job().also {
    object : CoroutineScope {
        override val coroutineContext: CoroutineContext = it + EmptyCoroutineContext
        fun doInScope() {
            val blockName = coroutineName ?: (if (uiDispatcher) {
                "launchNewScopeUIBlock"
            } else {
                "launchNewScopeIOBlock"
            })
            launchBlock(uiDispatcher, blockName, completionHandler, block)
        }
    }.doInScope()
}

private fun CoroutineScope.launchBlock(uiDispatcher: Boolean = true, coroutineName: String? = null, completionHandler: CompletionHandler? = null, block: suspend CoroutineScope.() -> Unit): Job {
    val coroutineContext = (if (uiDispatcher) Dispatchers.Default else Dispatchers.IO).let { coroutineDispatcher ->
        coroutineName?.let { coroutineDispatcher + CoroutineName(coroutineName) } ?: coroutineDispatcher
    }
    return launch(coroutineContext) {
        block()
    }.also { job ->
        completionHandler?.let {
            job.invokeOnCompletion(it)
        }
    }
}