package com.aws.sa.receptionist.util

import com.aws.sa.receptionist.aws.lambda.LambdaUtils
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.ConfirmationStatus
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.Intent
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.LexRequest
import kotlinx.coroutines.experimental.delay
import org.apache.log4j.Logger

object Heartbeat {
    private val LOGGER = Logger.getLogger(Heartbeat::class.java)
    fun start(intervalInMinutes: Int = 10) {
        launchNewScopeIOBlock {
            while (true) {
                val lexRequest = LexRequest()
                val currentIntent = Intent("Heartbeat", null, null, ConfirmationStatus.None)
                lexRequest.currentIntent = currentIntent
                val lexResponseClose = LambdaUtils.executeJoe(lexRequest)
                LOGGER.info(lexResponseClose.toString())
                delay((1000 * 60 * intervalInMinutes).toLong())
            }
        }
    }
}