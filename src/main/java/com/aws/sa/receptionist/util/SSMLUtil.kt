package com.aws.sa.receptionist.util

object SSMLUtil {
    fun trim(ssml: String): String = ssml.replace("<[^>]+>".toRegex(), "")
    fun toSSML(text: String): String =
            if (text.startsWith("""<speak>""")) {
                text
            } else {
                """<speak>$text</speak>"""
            }
}