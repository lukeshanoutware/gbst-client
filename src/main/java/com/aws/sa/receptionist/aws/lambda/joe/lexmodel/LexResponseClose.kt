package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class LexResponseClose(var dialogAction: DialogActionClose? = null,
                            var sessionAttributes: Map<String, String>? = null)