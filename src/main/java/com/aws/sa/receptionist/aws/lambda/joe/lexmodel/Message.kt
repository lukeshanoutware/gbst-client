package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class Message(val contentType: ContentType = ContentType.PlainText,
                   val content: String = "") {

    companion object {

        fun plainText(content: String): Message = Message(ContentType.PlainText, content)
    }
}
