package com.aws.sa.receptionist.aws.lambda.joe;

import com.amazonaws.services.lambda.invoke.LambdaFunction;
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.LexRequest;
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.LexResponseClose;

public interface JoeService {
    @LambdaFunction(functionName = "Joe")
    LexResponseClose execute(LexRequest lexRequest);
}
