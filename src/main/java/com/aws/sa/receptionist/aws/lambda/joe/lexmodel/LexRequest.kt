package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class LexRequest(var currentIntent: Intent = Intent(),
                      var bot: Bot = Bot(),
                      var userId: String = "",
                      var invocationSource: InvocationSource? = null,
                      var outputDialogMode: String? = null,
                      var messageVersion: String? = null,
                      var inputTranscript: String? = null,
                      var sessionAttributes: Map<String, String>? = null,
                      var requestAttributes: Map<String, String>? = null)