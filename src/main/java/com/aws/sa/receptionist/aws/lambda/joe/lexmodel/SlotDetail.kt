package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class SlotDetail(var resolutions: List<ResolutionValue>? = null,
                      var originalValue: String? = null)