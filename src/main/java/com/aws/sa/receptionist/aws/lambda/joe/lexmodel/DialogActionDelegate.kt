package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class DialogActionDelegate(var slots: Map<String, String?>? = null) : DialogAction(Type.Delegate)