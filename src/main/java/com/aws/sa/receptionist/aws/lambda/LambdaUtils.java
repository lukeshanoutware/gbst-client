package com.aws.sa.receptionist.aws.lambda;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.aws.sa.receptionist.aws.lambda.joe.JoeService;
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.LexRequest;
import com.aws.sa.receptionist.aws.lambda.joe.lexmodel.LexResponseClose;
import com.aws.sa.receptionist.config.Config;

public class LambdaUtils {
    private static final AWSLambda lambdaClient = AWSLambdaClientBuilder
            .standard()
            .withCredentials(new StaticCredentialsProvider(new BasicAWSCredentials(Config.AWS_KEY, Config.AWS_SECRET)))
            .withRegion(Regions.US_WEST_2).build();

    public static LexResponseClose executeJoe(LexRequest lexRequest) {
        final JoeService joeService = LambdaInvokerFactory
                .builder()
                .lambdaClient(lambdaClient)
                .build(JoeService.class);

        return joeService.execute(lexRequest);
    }
}
