package com.aws.sa.receptionist.aws.polly;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.polly.AmazonPolly;
import com.amazonaws.services.polly.AmazonPollyClientBuilder;
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest;
import com.amazonaws.services.polly.model.SynthesizeSpeechResult;
import com.amazonaws.services.polly.model.TextType;
import com.aws.sa.receptionist.audio.AudioListener;
import com.aws.sa.receptionist.config.Config;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PollyUtils {
    private static final Logger LOGGER = Logger.getLogger(PollyUtils.class);
    private static final PollyUtils instance = new PollyUtils();

    public static final PollyUtils getInstance() {
        return instance;
    }

    private PollyUtils() {
    }

    private final AmazonPolly amazonPolly = AmazonPollyClientBuilder.standard()
            .withCredentials(new StaticCredentialsProvider(new BasicAWSCredentials(Config.AWS_KEY, Config.AWS_SECRET)))
            .withRegion(Regions.US_WEST_2).build();

    private Map<String, byte[]> cache = Collections.synchronizedMap(new HashMap<>());

    /**
     * Make an async call to polly and play the result
     */
    public void playVoice(String text, AudioListener listener) {
        PollyRunnable runnable = new PollyRunnable(text, listener, this);
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }

    public byte[] createVoice(String text, AudioListener listener) {
        listener.creatingVoice();

        if (cache.containsKey(text)) {
            listener.voiceCreated();
            return cache.get(text);
        } else {
            SynthesizeSpeechRequest request = new SynthesizeSpeechRequest();
            request.setTextType(TextType.Ssml);
            request.setOutputFormat("mp3");
            request.setVoiceId(Config.VOICE);
            request.setSampleRate("22050");
            request.setText(text);

            SynthesizeSpeechResult result = amazonPolly.synthesizeSpeech(request);

            try (InputStream in = result.getAudioStream()) {
                byte[] voice = IOUtils.toByteArray(in);
                cache.put(text, voice);
                listener.voiceCreated();
                return voice;
            } catch (IOException e) {
                LOGGER.error("Failed to create voice", e);
                listener.voiceCreationFailed(e);
                return null;
            }
        }
    }
}
