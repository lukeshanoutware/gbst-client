package com.aws.sa.receptionist.aws.polly;

import com.aws.sa.receptionist.audio.AudioListener;
import com.aws.sa.receptionist.audio.AudioManager;

public class PollyRunnable implements Runnable {
    private final AudioListener listener;
    private final PollyUtils polly;
    private final String text;

    public PollyRunnable(String text, AudioListener listener, PollyUtils polly) {
        this.text = text;
        this.listener = listener;
        this.polly = polly;
    }

    @Override
    public void run() {
        AudioManager.getInstance().playAsynch(polly.createVoice(text, listener), listener);
    }
}
