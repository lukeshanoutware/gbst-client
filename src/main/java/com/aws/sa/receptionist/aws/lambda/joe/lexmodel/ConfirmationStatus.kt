package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

enum class ConfirmationStatus {
    None, Confirmed, Denied
}
