package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class Bot(val name: String = "",
               val version: String = "",
               val alias: String? = null)