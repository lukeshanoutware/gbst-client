package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

enum class ContentType {
    PlainText, SSML, CustomPayload
}
