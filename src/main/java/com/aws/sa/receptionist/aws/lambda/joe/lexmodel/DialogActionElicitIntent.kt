package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class DialogActionElicitIntent(var message: Message? = null,
                                    var responseCard: ResponseCard? = null) : DialogAction(Type.ElicitIntent)