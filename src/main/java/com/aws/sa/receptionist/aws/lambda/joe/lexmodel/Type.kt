package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

enum class Type {
    ElicitIntent, ElicitSlot, ConfirmIntent, Delegate, Close
}
