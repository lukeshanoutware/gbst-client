package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

data class DialogActionClose(val fulfillmentState: FulfillmentState = FulfillmentState.Fulfilled,
                             val message: Message? = null,
                             val responseCard: ResponseCard? = null) : DialogAction(Type.Close) {

    companion object {

        fun create(fulfillmentState: FulfillmentState, messageContent: String): DialogActionClose =
                DialogActionClose(fulfillmentState, Message.plainText(messageContent), null)
    }
}
