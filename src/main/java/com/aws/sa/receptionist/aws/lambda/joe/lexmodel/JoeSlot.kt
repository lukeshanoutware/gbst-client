package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

enum class JoeSlot(val slotName: String, val message: String) {
    FUND_ID_STRING("Funds", "What fund ID are you referring to?"),
    SALESPERSON_STRING("Salesperson", "Please specify the salesperson's ID."),
    CLIENT_STRING("Clients", "Which client are you referring to?"),
    MINUTES_INT("minutes", "how many minutes ago do you want to check the number of processed transactions?")
}