package com.aws.sa.receptionist.aws.lambda.joe.lexmodel

enum class FulfillmentState {
    Fulfilled, Failed
}
