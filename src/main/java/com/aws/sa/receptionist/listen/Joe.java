package com.aws.sa.receptionist.listen;

import com.amazonaws.services.lexruntime.model.PostContentResult;
import com.aws.sa.receptionist.audio.AudioListener;
import com.aws.sa.receptionist.aws.polly.PollyUtils;
import com.aws.sa.receptionist.config.Config;
import com.aws.sa.receptionist.context.BotDoneListener;
import com.aws.sa.receptionist.context.StateBot;
import com.aws.sa.receptionist.context.UserContext;
import com.aws.sa.receptionist.util.SSMLUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class Joe extends StateBot implements SpeechResultListener {
    private static final Logger LOGGER = Logger.getLogger(Joe.class);

    public Joe(UserContext context, BotDoneListener doneListener) {
        super(context, doneListener);
        context.joeIsRunning = true;
    }

    @Override
    public void innerRun() {
        if (!context.joeHasRun) {
            context.joeHasRun = true;
            PollyUtils.getInstance().playVoice(Config.GREETING, new AudioListener() {
                @Override
                public void audioStarting() {
                    context.getPanel().setText(SSMLUtil.INSTANCE.trim(Config.GREETING));
                }

                @Override
                public void audioComplete() {
                    if (!context.walkeyTalkeyMode) {
                        doIt();
                    } else {
                        finishConversation();
                    }
                }
            });
        } else {
            doIt();
        }
    }

    private void doIt() {
        context.getPanel().setRecording(true);
        context.getPanel().setLexing(false);
        context.getPanel().getMicListener().startListening(MicListener.ListeningMode.GBST_JOE, this);
        context.getPanel().setText(Config.LISTENING);
    }

    @Override
    public void finishedListening() {
        context.getPanel().setRecording(false);
        context.getPanel().setLexing(true);
        context.getPanel().setText(Config.THINKING);
    }

    @Override
    public void cancelledListening() {
        finishConversation();
    }

    @Override
    public void accept(byte[] responseMp3, String responseText, PostContentResult result) {
        if (result != null) {
            LOGGER.info("inputTranscript: " + result.getInputTranscript());
            LOGGER.info("Intent: " + result.getIntentName());
            LOGGER.info("Slots: " + result.getSlots());
        }
        if (responseText != null) {
            LOGGER.info("Response from Joe Lex:" + responseText);
            context.getPanel().setText(SSMLUtil.INSTANCE.trim(responseText));
        }
//        if (responseMp3 != null && responseMp3.length > 0 && StringUtils.isNotBlank(responseText)) {
//            AudioManager.getInstance().playAsynch(responseMp3, new AudioListener() {
//                @Override
//                public void audioComplete() {
//                    process(result);
//                }
//            });
//        }
        if (StringUtils.isNotBlank(responseText)) {
            PollyUtils.getInstance().playVoice(SSMLUtil.INSTANCE.toSSML(responseText), new AudioListener() {
                @Override
                public void audioComplete() {
                    process(result);
                }
            });
        } else {
            process(result);
        }
    }

    private void process(PostContentResult result) {
        if (!context.joeIsRunning ||
                (result != null && (result.getDialogState().equals("Fulfilled") || result.getDialogState().equals("Failed")))) {
            sleep(Config.PAUSE_BEFORE_FINISH_MILLI_SECONDS);
            finishConversation();
        } else {
            if (result != null) {
                context.lexSessionAttributes = result.getSessionAttributes();
            }
            innerRun();
        }
    }

    private void finishConversation() {
        context.joeIsRunning = false;
        context.lexSessionAttributes = null;
        context.getPanel().setRecording(false);
        context.getPanel().setLexing(false);
        context.getPanel().reset();
        done();
    }
}
