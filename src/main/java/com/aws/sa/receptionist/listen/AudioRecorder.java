package com.aws.sa.receptionist.listen;

import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import com.aws.sa.receptionist.config.Config;
import org.apache.log4j.Logger;

import javax.sound.sampled.AudioFormat;
import java.io.ByteArrayOutputStream;

public class AudioRecorder implements AudioProcessor {
    private static final Logger LOGGER = Logger.getLogger(AudioRecorder.class);
    public static final double DEFAULT_SILENCE_THRESHOLD = Config.SOUND_LEVEL_SILENCE;//db
    private double threshold = DEFAULT_SILENCE_THRESHOLD;//db

    private ByteArrayOutputStream bos = new ByteArrayOutputStream();
    private AudioFormat format;
    private long startTime = -1;
    private long firstVoiceTime = -1;
    private long lastVoiceTime = -1;
    private MicListener listener;
    private boolean listening = false;

    public AudioRecorder(AudioFormat format, MicListener listener) {
        this.format = format;
        this.listener = listener;
    }

    public void start() {
        startTime = -1;
        firstVoiceTime = -1;
        lastVoiceTime = -1;
        bos = new ByteArrayOutputStream();
        listening = true;
    }

    private void stop() {
        listening = false;
        bos = new ByteArrayOutputStream();
    }

    public void processingFinished() {
    }

    private void sendToLex(boolean markJoeRunning) {
        byte[] bytes = bos.toByteArray();
        stop();
        listener.publishToLex(bytes);
        listener.context.joeIsRunning = markJoeRunning;
    }

    private void cancel() {
        stop();
        listener.cancel();
    }

    private void onRecording(boolean isSilence, long diffFromStartTime, long diffFromFirstVoiceTime, long diffFromLastVoiceTime) {
        boolean userHasStoppedTalking = isSilence && diffFromLastVoiceTime > Config.MAX_SILENCE_MILLI_SECONDS;
        boolean overMaxSentenceLength = diffFromFirstVoiceTime > Config.MAX_SENTENCE_LENGTH_MILLI_SECONDS;
        if (!listener.context.walkeyTalkeyMode) {
            if (userHasStoppedTalking || overMaxSentenceLength) {
                sendToLex(!listener.context.walkeyTalkeyMode);
            }
        }
    }

    private void onButtonClickedWhenRecording() {
        if (!listener.context.walkeyTalkeyMode) {
            cancel();
        } else {
            sendToLex(!listener.context.walkeyTalkeyMode);
        }
    }

    /**
     * We only start filling the buffer after user start talking
     */
    public boolean process(AudioEvent audioEvent) {
        if (listener.context.joeIsRunning) {
            if (listening) {
                long currentTime = System.currentTimeMillis();
                if (startTime < 0) {
                    startTime = currentTime;
                }
                boolean isSilence = isSilence(audioEvent.getFloatBuffer());
                if (!isSilence) {
                    lastVoiceTime = currentTime;
                    if (firstVoiceTime < 0) {
                        firstVoiceTime = currentTime;
                    }
                }
                if (firstVoiceTime > 0) {
                    int byteOverlap = audioEvent.getOverlap() * format.getFrameSize();
                    int byteStepSize = audioEvent.getBufferSize() * format.getFrameSize() - byteOverlap;
                    if (audioEvent.getTimeStamp() == 0) {
                        byteOverlap = 0;
                        byteStepSize = audioEvent.getBufferSize() * format.getFrameSize();
                    }
                    bos.write(audioEvent.getByteBuffer(), byteOverlap, byteStepSize);
                }

                long diffFromStartTime = startTime < 0 ? 0 : currentTime - startTime;
                long diffFromFirstVoiceTime = firstVoiceTime < 0 ? 0 : currentTime - firstVoiceTime;
                long diffFromLastVoiceTime = lastVoiceTime < 0 ? 0 : currentTime - lastVoiceTime;
                onRecording(isSilence, diffFromStartTime, diffFromFirstVoiceTime, diffFromLastVoiceTime);
            }
        } else {
            if (listening) {
                onButtonClickedWhenRecording();
            }
        }
        return true;
    }

    /**
     * Calculates the local (linear) energy of an audio buffer.
     *
     * @param buffer The audio buffer.
     * @return The local (linear) energy of an audio buffer.
     */
    private double localEnergy(final float[] buffer) {
        double power = 0.0D;
        for (float element : buffer) {
            power += element * element;
        }
        return power;
    }

    /**
     * Returns the dBSPL for a buffer.
     *
     * @param buffer The buffer with audio information.
     * @return The dBSPL level for the buffer.
     */
    private double soundPressureLevel(final float[] buffer) {
        double value = Math.pow(localEnergy(buffer), 0.5);
        value = value / buffer.length;
        return linearToDecibel(value);
    }

    /**
     * Converts a linear to a dB value.
     *
     * @param value The value to convert.
     * @return The converted value.
     */
    private double linearToDecibel(final double value) {
        return 20.0 * Math.log10(value);
    }

    /**
     * Checks if the dBSPL level in the buffer falls below a certain threshold.
     *
     * @param buffer           The buffer with audio information.
     * @param silenceThreshold The threshold in dBSPL
     * @return True if the audio information in buffer corresponds with silence,
     * false otherwise.
     */
    public boolean isSilence(final float[] buffer, final double silenceThreshold) {
        return soundPressureLevel(buffer) < silenceThreshold;
    }

    public boolean isSilence(final float[] buffer) {
        return isSilence(buffer, threshold);
    }
}