package com.aws.sa.receptionist.listen;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.io.jvm.JVMAudioInputStream;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lexruntime.AmazonLexRuntime;
import com.amazonaws.services.lexruntime.AmazonLexRuntimeClientBuilder;
import com.amazonaws.services.lexruntime.model.PostContentRequest;
import com.amazonaws.services.lexruntime.model.PostContentResult;
import com.aws.sa.receptionist.config.Config;
import com.aws.sa.receptionist.context.UserContext;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MicListener {
    private static final Logger LOGGER = Logger.getLogger(MicListener.class);
    public UserContext context;
    private AmazonLexRuntime client;
    private AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, false);
    private AudioRecorder recorder = new AudioRecorder(format, this);

    private ListeningMode mode = null;
    private SpeechResultListener resultListener = null;
    private Thread thread;

    public void cancel() {
        LOGGER.info("Cancel Listening");
        if (resultListener != null) {
            resultListener.cancelledListening();
        }
    }

    public void init(UserContext context) throws LineUnavailableException {
        this.context = context;
        client = AmazonLexRuntimeClientBuilder.standard()
                .withCredentials(new StaticCredentialsProvider(new BasicAWSCredentials(Config.AWS_KEY, Config.AWS_SECRET)))
                .withRegion(Regions.US_WEST_2).build();

        DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
        TargetDataLine microphone = (TargetDataLine) AudioSystem.getLine(info);
        microphone.open(format);
        microphone.start();

        final AudioInputStream inputStream = new AudioInputStream(microphone);
        JVMAudioInputStream stream = new JVMAudioInputStream(inputStream);
        final AudioDispatcher dispatcher = new AudioDispatcher(stream, 1024, 0);
        dispatcher.addAudioProcessor(recorder);
        //dispatcher.addAudioProcessor(new SilenceDetector());
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                dispatcher.run();
            }
        }, "Audio Listener");
        thread.start();
    }

    public void startListening(ListeningMode mode, SpeechResultListener resultListener) {
        LOGGER.info("Started Listening...");
        this.mode = mode;
        this.resultListener = resultListener;
        this.recorder.start();
    }

    @SuppressWarnings("unchecked")
    void publishToLex(byte[] audioBytes) {
        if (resultListener != null) {
            resultListener.finishedListening();
        }

        PostContentRequest req = new PostContentRequest()
                .withContentType("audio/lpcm; sample-rate=8000; sample-size-bits=16; channel-count=1; is-big-endian=false")
                .withBotAlias(mode.botAlias)
                .withBotName(mode.botName)
                .withUserId(Config.ASSISTANT_NAME)
                .withSessionAttributes(context.lexSessionAttributes)
                .withAccept("text/plain; charset=utf-8")
                .withInputStream(new ByteArrayInputStream(audioBytes));

        LOGGER.info("Sending to Lex: " + mode.name());
        try {
            PostContentResult result = client.postContent(req);
            if (resultListener != null) {
                InputStream audioStream = result.getAudioStream();

                byte[] bytes = null;
                try {
                    bytes = audioStream != null ? IOUtils.toByteArray(audioStream) : null;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                resultListener.accept(bytes, result.getMessage(), result);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (resultListener != null) {
                resultListener.accept(null, Config.PARDON, null);
            }
        }
    }

    public enum ListeningMode {
        GBST_JOE(Config.LEX_NAME, Config.LEX_ALIAS);

        String botName;
        String botAlias;

        ListeningMode(String botName, String botAlias) {
            this.botAlias = botAlias;
            this.botName = botName;
        }
    }
}
