package com.aws.sa.receptionist.listen;

import com.amazonaws.services.lexruntime.model.PostContentResult;

public interface SpeechResultListener {
    void accept(byte[] responseMp3, String responseText, PostContentResult result);

    void finishedListening();

    void cancelledListening();
}
