package com.aws.sa.receptionist.ui;

import com.aws.sa.receptionist.config.Config;

import javax.swing.*;
import java.awt.*;

public class ReceptionistFrame extends JFrame {
    private CameraPanel cameraPanel = new CameraPanel();

    public ReceptionistFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setTitle(Config.APP_NAME);
        setBackground(Color.white);
        getRootPane().setBackground(Color.white);
        initializeSize();
    }

    public void initializeSize() {
        if (Config.fullscreen) {
            setSize(Toolkit.getDefaultToolkit().getScreenSize());
            setExtendedState(JFrame.MAXIMIZED_BOTH);
            setUndecorated(true);
        } else {
            setSize(960, 240);
        }

        add(cameraPanel, BorderLayout.CENTER);

        setLocationRelativeTo(null);
    }

}
