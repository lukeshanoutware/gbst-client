package com.aws.sa.receptionist.ui;

import com.aws.sa.receptionist.audio.AudioListener;
import com.aws.sa.receptionist.aws.polly.PollyUtils;
import com.aws.sa.receptionist.config.Config;
import com.aws.sa.receptionist.context.UserContext;
import com.aws.sa.receptionist.flow.FlowManager;
import com.aws.sa.receptionist.listen.MicListener;
import com.aws.sa.receptionist.util.SSMLUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class CameraPanel extends JPanel implements ActionListener {
    private static final Logger LOGGER = Logger.getLogger(CameraPanel.class);

    private long iteration = 0L;

    private String text = SSMLUtil.INSTANCE.trim(Config.WELCOME);

    private MicListener listener = new MicListener();
    private final Timer renderTimer = new Timer(50, this);
    private final Font font = new Font("Arial", Font.BOLD, 24);

    private BufferedImage gbstLogo = null;
    private ImageIcon microphoneRecording = null;
    private ImageIcon microphoneIdle = null;
    private boolean flowIsRunning = true;

    private UserContext context;
    private JButton assistantButton;
    private JCheckBox walkieTalkieMode;

    private boolean recording = false;

    public CameraPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        context = new UserContext(this);
        setBackground(Color.white);
        try {
            listener.init(context);
        } catch (LineUnavailableException e) {
            LOGGER.error("Failed to listen on microphone", e);
        }
        loadIcons();

        walkieTalkieMode = new JCheckBox();
        walkieTalkieMode.setVisible(false);
        walkieTalkieMode.setText("Walkie Talkie Mode");
        walkieTalkieMode.setSelected(context.walkeyTalkeyMode);
        walkieTalkieMode.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.DESELECTED) {
                    context.walkeyTalkeyMode = false;
                } else {
                    context.walkeyTalkeyMode = true;
                }
            }
        });
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 2;
        c.gridy = 0;
        c.gridwidth = 1;
        c.weightx = 0;
        c.weighty = 0;
        add(walkieTalkieMode, c);

        assistantButton = new JButton(microphoneIdle);
        assistantButton.setPressedIcon(microphoneIdle);
        assistantButton.setBorder(null);
        assistantButton.setBackground(Color.white);
        assistantButton.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (context.walkeyTalkeyMode && assistantButton.isEnabled()) {
                    startFlow();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (context.walkeyTalkeyMode && assistantButton.isEnabled()) {
                    stopFlow();
                }
            }
        });
        assistantButton.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (context.walkeyTalkeyMode && assistantButton.isEnabled()) {
                    startFlow();
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (context.walkeyTalkeyMode && assistantButton.isEnabled()) {
                    stopFlow();
                }
            }
        });
        assistantButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!context.walkeyTalkeyMode && assistantButton.isEnabled()) {
                    if (!context.joeIsRunning) {
                        startFlow();
                    } else {
                        stopFlow();
                    }
                }
            }
        });
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.NORTH;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 3;
        c.weightx = 1;
        c.weighty = 1;
        c.ipady = 32;
        add(assistantButton, c);

        renderTimer.start();
    }

    private void startFlow() {
        if (!flowIsRunning) {
            flowIsRunning = true;
            FlowManager.run(context);
        }
    }

    private void stopFlow() {
        flowIsRunning = false;
        context.joeIsRunning = false;
    }

    private void loadIcons() {
        try {
            gbstLogo = ImageIO.read(getClass().getResourceAsStream("/images/coexs.jpg"));
            microphoneIdle = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/images/mic7.gif")));
            ClassLoader cldr = getClass().getClassLoader();
            java.net.URL imageURL = cldr.getResource("images/mic5.gif");
            microphoneRecording = new ImageIcon(imageURL);
        } catch (IOException e) {
            LOGGER.error("Failed to load image", e);
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
        if (recording) {
            assistantButton.setIcon(microphoneRecording);
            assistantButton.setPressedIcon(microphoneRecording);
            assistantButton.setBorder(null);
            assistantButton.setBackground(Color.white);
            walkieTalkieMode.setEnabled(false);
        } else {
            assistantButton.setIcon(microphoneIdle);
            assistantButton.setPressedIcon(microphoneIdle);
            assistantButton.setBorder(null);
            assistantButton.setBackground(Color.white);
            walkieTalkieMode.setEnabled(true);
        }
    }

    public void setLexing(boolean lexing) {
        if (lexing) {
            assistantButton.setEnabled(false);
            walkieTalkieMode.setEnabled(false);
        } else {
            assistantButton.setEnabled(true);
            walkieTalkieMode.setEnabled(!recording);
        }
    }

    public MicListener getMicListener() {
        return listener;
    }

    public void reset() {
        setText(context.walkeyTalkeyMode ? Config.PROMPT_TO_ASK_WALKIE_TALKIE_MODE : Config.PROMPT_TO_ASK);
        flowIsRunning = false;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (gbstLogo != null) {
            g.drawImage(gbstLogo, 8, 8, null);
        }

        if (StringUtils.isNotBlank(text)) {
            g.setFont(font);

            g.setColor(new Color(0, 0, 0, 155));
            g.fillRect(0, getHeight() - 60, getWidth(), 60);

            g.setColor(Color.WHITE);
            g.drawString(text, 8, getHeight() - 20);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        iteration++;

        if (iteration == 1) {
            PollyUtils.getInstance().playVoice(Config.WELCOME, new AudioListener() {
                @Override
                public void audioComplete() {
                    flowIsRunning = false;
                    startFlow();
                }
            });
        }

        invalidate();
        repaint();
    }
}
