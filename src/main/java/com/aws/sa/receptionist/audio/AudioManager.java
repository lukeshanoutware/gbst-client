package com.aws.sa.receptionist.audio;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;

public class AudioManager
{
    private static final AudioManager INSTANCE = new AudioManager();

    public static AudioManager getInstance()
    {
        return INSTANCE;
    }

    private AudioManager()
    {

    }

    public void play(byte [] data)
    {
        if (data == null || data.length == 0) {
            return;
        }

        try (final InputStream bytesIn = new ByteArrayInputStream(data);
             final AudioInputStream in = getAudioInputStream(bytesIn))
        {
            final AudioFormat outFormat = getOutFormat(in.getFormat());
            final DataLine.Info info = new DataLine.Info(SourceDataLine.class, outFormat);

            try (final SourceDataLine line =
                         (SourceDataLine) AudioSystem.getLine(info))
            {

                if (line != null)
                {
                    line.open(outFormat);
                    line.start();
                    stream(getAudioInputStream(outFormat, in), line);
                    line.drain();
                    line.stop();
                }
            }

        } catch (UnsupportedAudioFileException
                | LineUnavailableException
                | IOException e)
        {
            throw new IllegalStateException(e);
        }
    }

    private AudioFormat getOutFormat(AudioFormat inFormat)
    {
        final int ch = inFormat.getChannels();
        final float rate = inFormat.getSampleRate();
        return new AudioFormat(PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
    }

    private void stream(AudioInputStream in, SourceDataLine line)
            throws IOException
    {
        final byte[] buffer = new byte[65536];
        for (int n = 0; n != -1; n = in.read(buffer, 0, buffer.length))
        {
            line.write(buffer, 0, n);
        }
    }

    /**
     * Plays a sound asynchronously
     */
    public void playAsynch(byte [] data, AudioListener listener)
    {
        if (data == null)
        {
            return;
        }

        Thread thread = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                listener.audioStarting();
                play(data);
                listener.audioComplete();
            }
        });

        thread.setDaemon(true);
        thread.start();
    }


}
