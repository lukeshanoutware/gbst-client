package com.aws.sa.receptionist.audio;

import org.apache.log4j.Logger;

public interface AudioListener
{
    default void audioStarting() {};
    default void audioComplete() {};

    default void creatingVoice() {};
    default void voiceCreated() {};
    default void voiceCreationFailed(Throwable cause) {
    		Logger.getLogger(AudioListener.class).error(cause, cause.getCause());
    };
}