package com.aws.sa.receptionist.context;

import com.aws.sa.receptionist.config.Config;
import com.aws.sa.receptionist.ui.CameraPanel;

public class UserContext {
    public boolean joeHasRun = false;
    public boolean joeIsRunning = false;
    public boolean walkeyTalkeyMode = Config.WALKIE_TALKIE_MODE;
    public String lexSessionAttributes;
    private CameraPanel panel;

    public UserContext(CameraPanel panel) {
        this.panel = panel;
    }

    public CameraPanel getPanel() {
        return panel;
    }
}
