package com.aws.sa.receptionist.context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class StateBot implements Runnable, BotDoneListener {
	private static final ExecutorService executors = Executors.newCachedThreadPool();

	protected UserContext context;
	protected BotDoneListener doneListener;
	
	public StateBot(UserContext context, BotDoneListener doneListener) {
		this.context = context; this.doneListener = doneListener;
	}
	
	protected abstract void innerRun();
	
	@Override
	public void run() {
		executors.submit(() -> {
			innerRun();
		});
	}
	
	protected void done() {
		if (doneListener != null) {
			doneListener.accept(context);
		}
	}
	
	protected void sleep(long millis) {
		try { Thread.sleep(millis); } catch(InterruptedException e) { }
	}
	
	public void setDoneListener(BotDoneListener doneListener) {
		this.doneListener = doneListener;
	}
	
	@Override
    public void accept(UserContext context) {
    		run();
    }
}
