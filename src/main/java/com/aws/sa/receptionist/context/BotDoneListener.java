package com.aws.sa.receptionist.context;

public interface BotDoneListener {
	void accept(UserContext context);
}
