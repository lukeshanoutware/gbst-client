package com.aws.sa.receptionist;

import com.aws.sa.receptionist.config.Config;
import com.aws.sa.receptionist.ui.ReceptionistFrame;
import com.aws.sa.receptionist.util.Heartbeat;

public class Receptionist {

    public static void main(String[] args) {
        ReceptionistFrame frame = new ReceptionistFrame();
        frame.setVisible(true);
        Heartbeat.INSTANCE.start(Config.HEARTBEAT_INTERVAL_MINUTES);
    }
}
